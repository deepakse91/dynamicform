import { Component, OnInit, TemplateRef } from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';



@Component({
  selector: 'app-add-template',
  templateUrl: './add-template.component.html',
  styleUrls: ['./add-template.component.css']
})
export class AddTemplateComponent implements OnInit {

  public Editor = ClassicEditor;
  category: any = [];

  questionSection: boolean = false;

  

  constructor() {}

  ngOnInit() {
   

    this.category = [
      {"id":1, "name": "Rental"},
      {"id":2, "name": "Affidavit"},
      {"id":3, "name": "Draft"}
    ]
  }


  

  firstStepClicked()
  {
    this.questionSection = true;
  }

  backStep()
  {
    this.questionSection = false;
  }


  deleteQuestion()
  {
    console.log('Removed');
  }

 


}
