import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CreateTemplateComponent } from './create-template/create-template.component';
import { AddTemplateComponent } from './add-template/add-template.component';


const routes: Routes = [
  { path: 'create', component: CreateTemplateComponent },
  {path: 'add-template', component:AddTemplateComponent }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []

})


export class AppRoutingModule { }
