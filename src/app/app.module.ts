import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ModalModule } from 'ngx-bootstrap/modal';

import { AppComponent } from './app.component';
import { CreateTemplateComponent } from './create-template/create-template.component';
import { AppRoutingModule } from './app-routing.module';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

import { FormsModule } from '@angular/forms';
import { AddTemplateComponent } from './add-template/add-template.component';


@NgModule({
  declarations: [
    AppComponent,
    CreateTemplateComponent,
    AddTemplateComponent
  ],
  imports: [
    BrowserModule, FormsModule,
    AppRoutingModule,
    CKEditorModule,
    ModalModule.forRoot()
  ],
  entryComponents:[AddTemplateComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
