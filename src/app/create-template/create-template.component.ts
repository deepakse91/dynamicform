import { Component, OnInit, ElementRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-create-template',
  templateUrl: './create-template.component.html',
  styleUrls: ['./create-template.component.css']
})
export class CreateTemplateComponent implements OnInit {

  constructor(private sanitizer: DomSanitizer) { }
  x: any;
  content: any;

  ngOnInit() {
    this.x = [{
      "type": "text",
      "placeholder": "Enter your name",
      "id": "name",
      "model": "x",
      "required": false
    }, {
      "type": "number",
      "placeholder": "Enter your age",
      "id": "age",
      "model": "y",
      "required": true
    }, {
      "type": "date",
      "placeholder": "Enter your dob",
      "id": "dob",
      "model": "z",
      "required": true
    }];
    this.content = `My name is 
    <span id="PH_name" name="txt" style="font-style:italic; color: #ff0000;">...........................</span> and My Date of Birth is  <span id="PH_dob">...........................</span>
     and I am <span id="PH_age">...........................</span> years old`;


    this.content = this.sanitizer.bypassSecurityTrustHtml(this.content);
  }


  public placeHolderChange(event, form: NgForm) {
    // console.log('I am here', form)

    console.log(event, event.target.id)

    const idControl = event.target.id;

    // console.log(form.getControl())
    // let spanId = window.document.getElementById("#PH_name");


    // console.log(document.getElementById("PH_name2").innerHTML\)


    document.getElementById(`PH_${idControl}`).innerHTML = event.target.value;
  }


}
